from random import randint

name = input("Hi! What is your name? ")

# Guess 1

month_number = randint(1, 12)
year_number = randint(1924, 2004)

print("Guess 1 :", name, "were you born in",
      month_number, "/", year_number, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

# Guess 2

month_number = randint(1, 12)
year_number = randint(1924, 2004)

print("Guess 2 :", name, "were you born in",
      month_number, "/", year_number, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

# Guess 3

month_number = randint(1, 12)
year_number = randint(1924, 2004)

print("Guess 1 :", name, "were you born in",
      month_number, "/", year_number, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("ARE YOU SERIOUS?! I can't believe it... I gotta' try again!")

# Guess 4

month_number = randint(1, 12)
year_number = randint(1924, 2004)

print("Guess 1 :", name, "were you born in",
      month_number, "/", year_number, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("This is getting a little suss! Let's try one more time...")

# Guess 5

month_number = randint(1, 12)
year_number = randint(1924, 2004)

print("Guess 1 :", name, "were you born in",
      month_number, "/", year_number, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("NOPE! I don't believe you, YOU'RE LYING! I DON'T SPEAK TO GOSH DARN LIARS!")
